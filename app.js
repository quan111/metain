if (!process.env.DEBUG) {
    require('dotenv-safe').config({
        allowEmptyValues: true,
        example: './.env-api'
    })
}

var createError = require('http-errors');
var express = require('express');
const cors = require('cors');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const helmet = require('helmet');

var indexRouter = require('./routes/index');
var apiRouter = require('./routes/api/contract');
const mwEncrypt = require('./middleware/encrypt');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(helmet());
app.use(express.static(path.join(__dirname, 'public')));
app.use("/scripts", express.static(__dirname + "/node_modules/web3.js-browser/build/"));

app.use(cors({
    origin: ['http://localhost:3000', 'http://localhost:3001'],
    optionsSuccessStatus: 200
}));

app.use('/api', apiRouter, mwEncrypt.encrypt);
app.use('/', indexRouter);

// wrong request
app.use((req, res) => {
    console.log('wprong request')
    res.send('You make a wrong request. Please try again!!!')
})

const PORT = 3001
app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}...`);
})