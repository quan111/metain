const DATA_LENGTH_DEFAULT = 1;

const DATA_LENGTH_EQ = "==";
const DATA_LENGTH_LE = "<=";
const DATA_LENGTH_LT = "<";
const DATA_LENGTH_GE = ">=";
const DATA_LENGTH_GT = ">";

const DATA_TYPE_NUMBER = "number";
const DATA_TYPE_STRING = "string";
const DATA_TYPE_BOOLEAN = "boolean";
const DATA_TYPE_OBJECT = "object";
const DATA_TYPE_EMAIL = "Email";

const checkDataInput = (data_value, data_type, data_length_Comparison, data_length) => {
    try {
        if(!data_value) 
            return false;

        if(typeof data_value == 'undefined') 
            return false;

        if(data_value == 'undefined') 
            return false;
        
        if(data_type){
            if(data_type === DATA_TYPE_NUMBER){
                if(typeof data_value != DATA_TYPE_NUMBER)
                    return false;
            } else if(data_type === DATA_TYPE_STRING){
                if(typeof data_value != DATA_TYPE_STRING)
                    return false;
            } else if(data_type === DATA_TYPE_BOOLEAN){
                if(typeof data_value != DATA_TYPE_BOOLEAN)
                    return false;
            } else if(data_type === DATA_TYPE_OBJECT){
                if(typeof data_value != DATA_TYPE_OBJECT)
                    return false;
            } else if(data_type === DATA_TYPE_EMAIL){
                if(!validateEmail(data_value)) 
                    return false;
            }
        }

        var data_value_length = 0;
        if(typeof data_value == DATA_TYPE_STRING)
            data_value_length = data_value.length;
        else if (typeof data_value == DATA_TYPE_NUMBER)
            data_value_length = (data_value+"").length;
        else if (typeof data_value == DATA_TYPE_OBJECT)
            data_value_length = Object.keys(data_value).length;
        else
            data_value_length = 1;

        if(data_length && data_length_Comparison){
            if(data_length_Comparison == DATA_LENGTH_EQ) {
                if(data_value_length != data_length) 
                    return false; 
            }
            else if(data_length_Comparison == DATA_LENGTH_LE) {
                if(data_value_length > data_length) 
                    return false; 
            }
            else if(data_length_Comparison == DATA_LENGTH_LT) {
                if(data_value_length >= data_length) 
                    return false; 
            }
            else if(data_length_Comparison == DATA_LENGTH_GE) {
                if(data_value_length < data_length) 
                    return false; 
            }
            else if(data_length_Comparison == DATA_LENGTH_GT) {
                if(data_value_length <= data_length) 
                    return false; 
            }  
        } else {
            if(data_value_length < DATA_LENGTH_DEFAULT) 
                return false;
        }
    } catch (err) {
        console.debug("******** security: error: "+err);
        return false;
    }
    return true;
};

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

const reFormatEmail = (email) => {
    try {
        if (!email)
            return "";

        if (typeof email == 'undefined')
            return "";

        email = email.toLowerCase();
    } catch (err) {
        return "";
    }
    return email;
};

module.exports = {
    checkDataInput: checkDataInput,
    reFormatEmail: reFormatEmail,
    DATA_TYPE_NUMBER: DATA_TYPE_NUMBER,
    DATA_TYPE_STRING: DATA_TYPE_STRING,
    DATA_TYPE_BOOLEAN: DATA_TYPE_BOOLEAN,
    DATA_TYPE_OBJECT: DATA_TYPE_OBJECT,
    DATA_TYPE_EMAIL: DATA_TYPE_EMAIL,
    DATA_LENGTH_EQ: DATA_LENGTH_EQ,
    DATA_LENGTH_LE: DATA_LENGTH_LE,
    DATA_LENGTH_LT: DATA_LENGTH_LT,
    DATA_LENGTH_GE: DATA_LENGTH_GE,
    DATA_LENGTH_GT: DATA_LENGTH_GT,
}