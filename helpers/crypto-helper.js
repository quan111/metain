const crypto = require('crypto');
const AES = require('crypto-js/aes')
const UTF8 = require('crypto-js/enc-utf8')

const encrypt = (data, key, iv) => {
    key = crypto.createHash('sha256').update(String(key)).digest('base64').substr(0, 32).toString()
    const cipher = crypto.createCipheriv('aes256', key, iv)
    let result = cipher.update(data, 'utf8', 'hex')
    result += cipher.final('hex')
    return result
}

const decrypt = (data, key, iv) => {
    try {
        key = crypto.createHash('sha256').update(String(key)).digest('base64').substr(0, 32).toString()
        const decipher = crypto.createDecipheriv('aes256', key, iv)
        let result = decipher.update(data, 'hex', 'utf8')
        result += decipher.final('utf8')

        return result
    } catch (err) {
        console.log(err)
        return undefined
    }
}

const encryptAES = (data, key, iv) => {
    return AES.encrypt(data, key, { iv: iv }).toString();
}

const decryptAES = (data, key, iv) => {
    return AES.decrypt(data, key, { iv: iv }).toString(UTF8)
}

module.exports = {
    encrypt,
    decrypt,
    encryptAES,
    decryptAES
}
