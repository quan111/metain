const helperToken = require('./helper/api-token')
const security = require('../../helpers/security')
    // const User = require('../../models/UserDB')

const login = async(req, res, next) => {
    try {
        const id = req.body.id;

        if (!security.checkDataInput(id, security.DATA_TYPE_STRING))
            return goNext(400, {
                error: 'AUTHEN_BODY_WRONG_FORMAT'
            }, res, next);

        // let userItem = await User.findByID(id);
        // if (userItem && Object.keys(userItem).length) {
        //     const accessToken = await helperToken.generateAccessToken(id);
        //     const refreshToken = await helperToken.generateRefreshToken(id);
        //     goNext(200, {
        //         access_token: accessToken,
        //         refresh_token: refreshToken
        //     }, res, next)
        // } else {
        //     return goNext(400, {
        //         error: 'LOGIN FAIL'
        //     }, res, next);
        // }
    } catch (err) {
        console.log("DEBUG --------------- login err: " + err)
        return goNext(500, {
            error: 'SERVER_ERROR'
        }, res, next)
    }
}

const refreshToken = async(req, res, next) => {
    try {
        const refreshToken = req.body.refresh_token;
        if (!security.checkDataInput(refreshToken, security.DATA_TYPE_STRING))
            return goNext(400, {
                error: 'AUTHEN_BODY_WRONG_FORMAT'
            }, res, next);

        // const id = await helperToken.verifyRefreshToken(refreshToken);
        // let userItem = id ? await User.findByID(id) : {};

        // if (userItem && Object.keys(userItem).length) {
        //     const accessToken = await helperToken.generateAccessToken(id);
        //     goNext(200, {
        //         access_token: accessToken
        //     }, res, next)
        // } else {
        //     return goNext(400, {
        //         error: 'REFRESH_INVALID_TOKEN'
        //     }, res, next);
        // }
    } catch (err) {
        console.log('Debug ------------------- refreshToken: exception: ' + err);
        return goNext(500, {
            error: 'SERVER_ERROR'
        }, res, next)
    }
}

const validateAPI = async(accessToken) => {

    try {
        if (!security.checkDataInput(accessToken, security.DATA_TYPE_STRING))
            return {
                error: 'AUTHEN_INVALID_TOKENS_IN_HEADER'
            };

        // const id = await helperToken.verifyAccessToken(accessToken);
        // let userItem = id ? await User.findByID(id) : {};

        // if (userItem && Object.keys(userItem).length) {
        //     return {
        //         id: id
        //     };
        // } else {
        //     return {
        //         error: 'AUTHEN_VERIFY_ACCESS_TOKEN_FAILURE'
        //     };
        // }
    } catch (err) {
        console.log('Debug ------------------- validateAPI: exception: ' + err);
        return {
            error: 'SERVER_ERROR'
        }
    }
}

const goNext = (status, msg, res, next) => {
    console.log("DEBUG--------------goNext status: " + status + " msg: " + JSON.stringify(msg));
    res.statusCode = status
    res.msg = msg
    next()
}

module.exports = {
    apiLogin: login,
    apiRefreshToken: refreshToken,
    apiValidateAPI: validateAPI,
}