const jwt = require('jsonwebtoken');
const cryptoHelper = require('../../crypto-helper');

const generateAccessToken = (id) => {
	return new Promise((resolve, reject) => {
		jwt.sign(
			{ data: cryptoHelper.encrypt(id, process.env.SECRET_KEY_ACCESS_TOKEN, process.env.IV_ACCESS_TOKEN) },
			process.env.SECRET_KEY_ACCESS_TOKEN,
			{
				algorithm: 'HS256',
				expiresIn: process.env.EXPIRE_ACCESS_TOKEN
			},
			(err, token) => {
				if (!err) {
					resolve(token)
				} else {
					reject(err)
				}
			}
		)
	})
}

const generateRefreshToken = (id) => {
	return new Promise((resolve, reject) => {
		jwt.sign(
			{ data: cryptoHelper.encrypt(id, process.env.SECRET_KEY_REFRESH_TOKEN, process.env.IV_REFRESH_TOKEN) },
			process.env.SECRET_KEY_REFRESH_TOKEN,
			{
				algorithm: 'HS256',
				expiresIn: process.env.EXPIRE_REFRESH_TOKEN
			},
			(err, token) => {
				if (!err) {
					resolve(token)
				} else {
					reject(err)
				}
			}
		)
	})
}

const verifyRefreshToken = (token) => {
	return new Promise(resolve => {
		jwt.verify(token, process.env.SECRET_KEY_REFRESH_TOKEN, (err, decoded) => {
			resolve(err ? undefined : cryptoHelper.decrypt(decoded.data, process.env.SECRET_KEY_REFRESH_TOKEN, process.env.IV_REFRESH_TOKEN))
		})
	})
}

const verifyAccessToken = (token) => {
	return new Promise((resolve, reject) => {
		jwt.verify(token, process.env.SECRET_KEY_ACCESS_TOKEN, (err, decoded) => {
			resolve(err ? undefined : cryptoHelper.decrypt(decoded.data, process.env.SECRET_KEY_ACCESS_TOKEN, process.env.IV_ACCESS_TOKEN))
		})
	})
}

module.exports = {
	generateAccessToken,
	generateRefreshToken,
	verifyRefreshToken,
	verifyAccessToken
}
