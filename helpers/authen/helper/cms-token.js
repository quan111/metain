const jwt = require('jsonwebtoken')
const cryptoHelper = require('../../crypto-helper')

const generateLoginToken = (email) => {
    return new Promise((resolve, reject) => {
        jwt.sign(
            { data: cryptoHelper.encryptAES(email, process.env.SECRET_KEY_LOGIN_TOKEN, process.env.IV_LOGIN_TOKEN) },
            process.env.SECRET_KEY_LOGIN_TOKEN,
            {
                algorithm: 'HS256',
                expiresIn: process.env.EXPIRE_LOGIN_TOKEN
            },
            (err, token) => {
                if (!err) {
                    resolve(token)
                } else {
                    reject(err)
                }
            }
        )
    })
}

const verifyLoginToken = (token) => {
    return new Promise(resolve => {
        jwt.verify(token, process.env.SECRET_KEY_LOGIN_TOKEN, (err, decoded) => {
            resolve(err ? undefined : cryptoHelper.decryptAES(decoded.data, process.env.SECRET_KEY_LOGIN_TOKEN, process.env.IV_LOGIN_TOKEN))
        })
    })
}

module.exports = {
    generateLoginToken,
    verifyLoginToken
}
