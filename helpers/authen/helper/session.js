const sha1 = require('sha1')
const sessionDB = require('../../../models/SessionDB');

const saveSession = async (session) => {
    await sessionDB.insertOne({ sessionID: sha1(session) });
}

const removeSession = async (session) => {
    await sessionDB.removeSession({ sessionID: sha1(session) });
};

const validateSession = async (session) => {
    return sessionDB.findAll({ sessionID: sha1(session) });
}

module.exports = {
    saveSession,
    removeSession,
    validateSession
}
