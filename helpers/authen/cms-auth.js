const tokenHelper = require('./helper/cms-token');
const security = require('../security');
const sessionHelper = require('./helper/session');
const cryptoHelper = require('../crypto-helper');

const login = async (req, res) => {
    try {
        const reqEmail = security.reFormatEmail(req.body.email);
        const reqPassword = req.body.password;

        if (!security.checkDataInput(reqEmail, security.DATA_TYPE_EMAIL, null, null)
            || !security.checkDataInput(reqPassword, security.DATA_TYPE_STRING, null, null)) {
            return res.status(403).json({ error: 'Invalid input for query param.' });
        }

        const validateResult = validateAccount(reqEmail, reqPassword);
        if (!validateResult) {
            return res.status(403).json({ error: 'Email or password is invalid!' });
        }
        const loginToken = await tokenHelper.generateLoginToken(reqEmail);
        await sessionHelper.saveSession(loginToken);

        res.json({
            login_token: loginToken
        });

    } catch (err) {
        return res.status(500).json({ error: 'Server error' })
    }
}

const logout = async (req, res) => {
    try {
        const loginToken = req.body.login_token;
        await sessionHelper.removeSession(loginToken);
        res.json({
            response: 'Logged out successfully!'
        });
    } catch (err) {
        return res.status(500).json({ error: 'Server error' })
    }
}

const validateAccount = async (email, pass) => {
    try {
        if (cryptoHelper.encrypt(email, process.env.SECRET_KEY_GEN, process.env.IV_GEN) == process.env.CMS_BASIC_USER_NAME &&
            cryptoHelper.encrypt(pass, process.env.SECRET_KEY_GEN, process.env.IV_GEN) == process.env.CMS_BASIC_PASS)
            return true;
        else
            return false
    } catch (err) {
        return false;
    }
}


const checkAuthen = async (req, res, next) => {
    try {
        const loginToken = req.body.login_token;

        if (!loginToken) {
            return res.status(401).json({ error: 'Login is not found. Please login again!' });
        }

        if (!await sessionHelper.validateSession(loginToken)) {
            return res.status(401).json({ error: 'Session expired. Please login again!' });
        }

        // check token
        const email = await tokenHelper.verifyLoginToken(loginToken);
        if (!email) {
            return res.status(401).json({ error: 'Login token is invalid. Please login again!' })
        }

        let account = cryptoHelper.encrypt(email, process.env.SECRET_KEY_GEN, process.env.IV_GEN) == process.env.CMS_BASIC_USER_NAME ? true : false;
        if (!account) {
            return res.status(401).json({ error: 'Account is not found!' })
        }
        next();

    } catch (err) {
        return res.status(500).json({ error: 'Server error' })
    }
}

module.exports = {
    cmsLogin: login,
    cmsLogout: logout,
    cmsAuthen: checkAuthen
}