const cryptoHelper = require('../helpers/crypto-helper')

const encrypt = (req, res) => {
    if (res.msg) {
        const msg = cryptoHelper.encrypt(JSON.stringify(res.msg), process.env.SECRET_KEY_REQUEST_TOKEN, process.env.IV_REQUEST_TOKEN)
        res.status(res.statusCode).send(msg)
    } else {
        res.status(404).send('You make a wrong request!')
    }
}

module.exports = {
    encrypt
}
