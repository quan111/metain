

$(document).ready(function(){

	const abi = [
		{
			"anonymous": false,
			"inputs": [
				{
					"indexed": false,
					"internalType": "address",
					"name": "wallet",
					"type": "address"
				},
				{
					"indexed": false,
					"internalType": "uint256",
					"name": "ethCount",
					"type": "uint256"
				},
				{
					"indexed": false,
					"internalType": "uint256",
					"name": "sum",
					"type": "uint256"
				},
				{
					"indexed": false,
					"internalType": "uint256",
					"name": "count",
					"type": "uint256"
				}
			],
			"name": "update_data",
			"type": "event"
		},
		{
			"anonymous": false,
			"inputs": [
				{
					"indexed": false,
					"internalType": "address",
					"name": "wallet",
					"type": "address"
				},
				{
					"indexed": false,
					"internalType": "uint256",
					"name": "sum",
					"type": "uint256"
				},
				{
					"indexed": false,
					"internalType": "uint256",
					"name": "count",
					"type": "uint256"
				}
			],
			"name": "update_test",
			"type": "event"
		},
		{
			"inputs": [],
			"name": "count",
			"outputs": [
				{
					"internalType": "uint256",
					"name": "",
					"type": "uint256"
				}
			],
			"stateMutability": "view",
			"type": "function"
		},
		{
			"inputs": [],
			"name": "send_ETH",
			"outputs": [],
			"stateMutability": "payable",
			"type": "function"
		},
		{
			"inputs": [],
			"name": "sum",
			"outputs": [
				{
					"internalType": "uint256",
					"name": "",
					"type": "uint256"
				}
			],
			"stateMutability": "view",
			"type": "function"
		},
		{
			"inputs": [],
			"name": "test_update",
			"outputs": [],
			"stateMutability": "nonpayable",
			"type": "function"
		}
	];

	// goerli network
	const contract_Address = "0xbb3106a93f18440717adC4eF0925d60188a9E4F2";

	if (typeof web3 == 'undefined') {
		console.log("There is not Web3");
	}

	const my_web3 = new Web3(window.ethereum);
	if (typeof window.ethereum !== 'undefined') {
		console.log('MetaMask is installed!');
	} else {
		console.log("MetaMask is not installed");
	}

	// contract
	var contract_MM = new my_web3.eth.Contract(abi, contract_Address);

	// infura
	var provider_infura = new Web3.providers.WebsocketProvider("wss://goerli.infura.io/ws/v3/70c474f3d3724b1ab87aa10663ad5652");
	var web3_infura = new Web3(provider_infura);
	var contract_infura = web3_infura.eth.Contract(abi, contract_Address);
	
	// alchemyapi
	var provider_alchemy = new Web3.providers.WebsocketProvider("wss://eth-goerli.alchemyapi.io/v2/RlnjOKcWDvoylp-0b3TtQGet8ahW9caV");
	var web3_alchemy = new Web3(provider_alchemy);
	var contract_alchemy = web3_alchemy.eth.Contract(abi, contract_Address);

	
	var currentAccount = "";

	$("#enableMetamask").click(function () {

		connectMM().then((data)=>{
			currentAccount = data[0];
			console.log(currentAccount);
		}).catch((err)=>{
			console.log(err);
		});

	})

	$("#btnSendETH").click(function () {

		console.log(contract_MM);
		console.log(currentAccount);
		console.log(contract_infura);

		// contract_MM = new my_web3.eth.Contract(abi, contract_Address);
		contract_MM.methods.send_ETH().send({
			from: currentAccount
		});

		contract_infura.events.update_data({filter:{},fromBlock:"latest"},function(error, data){
			if(error){
				console.log(error);
			} else {
				console.log(data);
	
				$("#tbinfo").append(`
					<tr class="row">
					<td class="col-sm-2 col-lg-offset-1">`+ data.returnValues[0] +`</td>
					<td class="col-sm-2 col-lg-offset-1">`+ data.returnValues[1] +`</td>
					<td class="col-sm-2 col-lg-offset-1">`+ data.returnValues[2] +`</td>
					<td class="col-sm-2 col-lg-offset-1">`+ data.returnValues[3] +`</td>
				  </tr>`
				)	

			}
		})
	})

	$("#btnSendETHalchemy").click(function () {

		console.log(contract_MM);
		console.log(currentAccount);
		console.log(contract_infura);

		// contract_MM = new my_web3.eth.Contract(abi, contract_Address);
		contract_MM.methods.send_ETH().send({
			from: currentAccount
		});

		contract_alchemy.events.update_data({filter:{},fromBlock:"latest"},function(error, data){
			if(error){
				console.log(error);
			} else {
				console.log(data);
	
				$("#tbinfoalchemy").append(`
					<tr class="row">
					<td class="col-sm-2 col-lg-offset-1">`+ data.returnValues[0] +`</td>
					<td class="col-sm-2 col-lg-offset-1">`+ data.returnValues[1] +`</td>
					<td class="col-sm-2 col-lg-offset-1">`+ data.returnValues[2] +`</td>
					<td class="col-sm-2 col-lg-offset-1">`+ data.returnValues[3] +`</td>
				  </tr>`
				)	

			}
		})


	})

});

async function connectMM(){
	const accounts = await ethereum.request({ method: 'eth_requestAccounts' });
	return accounts;
}