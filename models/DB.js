const AWS = require('aws-sdk');
const exportLogs = require('../exports/exports_log');
AWS.config.update(
    {
        region: process.env.AWS_REGION_VAR,
        maxRetries: 10,
        httpOptions: {
            timeout: 5000
        }
    });
const dynamo = new AWS.DynamoDB();
const client = new AWS.DynamoDB.DocumentClient(
    {
        region: process.env.AWS_REGION_VAR,
        maxRetries: 10,
        httpOptions: {
            timeout: 5000
        }
    });

// const console = require('../console_log')

module.exports.get = (params) => {
    return new Promise((resolve, reject) => {
        client.get(params, function (err, data) {
            if (err) {
                console.debug("DEBUG DB GET: " + params.TableName + " err: " + err);
                return reject(err);
            }
            else {
                console.debug("DEBUG DB GET SUCCESSFUL: " + params.TableName);
                resolve(data);
            }

        });
    });
};

module.exports.batchGet = (params) => {
    return new Promise((resolve, reject) => {
        client.batchGet(params, (err, data) => {
            if (!err) {
                console.debug("DEBUG DB BATCH GET SUCCESSFUL: " + params.TableName)
                resolve(data)
            } else {
                console.debug("DEBUG DB BATCH GET: " + params.TableName + " err: " + err)
                reject(err)
            }
        })
    })
}

module.exports.put = (params) => {
    return new Promise((resolve, reject) => {
        client.put(params, (err, data) => {
            if (err) {
                console.debug("DEBUG DB PUT: " + params.TableName + " err: " + err);
                return reject(err);
            }
            else {
                console.debug("DEBUG DB PUT SUCCESSFUL: " + params.TableName);
                exportLogs.exportsLogs("Update", params);
                resolve(data);
            }
        });
    });
};
module.exports.scan = (params) => {
    return new Promise((resolve, reject) => {
        client.scan(params, function (err, data) {
            if (err) {
                console.debug("DEBUG DB SCAN: " + params.TableName + " err: " + err);
                return reject(err);
            }
            else {
                console.debug("DEBUG DB SCAN SUCCESSFUL: " + params.TableName);
                resolve(data);
            }
        });
    });
};

module.exports.update = (params) => {
    return new Promise((resolve, reject) => {
        client.update(params, function (err, data) {
            if (err) {
                console.debug("DEBUG DB UPDATE: " + params.TableName + " err: " + err);
                return reject(err);
            }
            else {
                console.debug("DEBUG DB UPDATE SUCCESSFUL: " + params.TableName);
                exportLogs.exportsLogs("Update", params);
                resolve(data);
            }
        });
    });
};

module.exports.delete = (params) => {
    return new Promise((resolve, reject) => {
        client.delete(params, function (err, data) {
            if (err) {
                console.debug("DEBUG DB DEL: " + params.TableName + " err: " + err);
                return reject(err);
            }
            else {
                console.debug("DEBUG DB DELETE SUCCESSFUL: " + params.TableName);
                exportLogs.exportsLogs("Delete", params);
                resolve(data);
            }
        });
    });
};

module.exports.query = (params) => {
    return new Promise((resolve) => {
        client.query(params, function (err, data) {
            if (err) {
                console.debug("DEBUG DB QUERY: " + params.TableName + " err: " + err);
                return resolve('');
            }
            else {
                console.debug("DEBUG DB QUERY ALL SUCCESSFUL: " + params.TableName);
                resolve(data);
            }
        });
    });
};

module.exports.queryLimit = async (params, max) => {
    let lastEvaluatedKey = 'dummy'; // string must not be empty
    const itemsAll = {
        Items: [],
        Count: 0
    };
    let queryCount = 1;
    try {
        while (lastEvaluatedKey && itemsAll.Count < max) {
            console.log("DEBUG DB QUERY ALL ---  " + queryCount + "th WITH PARAMS: " + JSON.stringify(params));
            queryCount += 1;
            const data = await client.query(params).promise();
            itemsAll.Items.push(...data.Items);
            itemsAll.Count += data.Count;
            lastEvaluatedKey = data.LastEvaluatedKey;
            console.log("DEBUG DB QUERY ALL: data.Count--- " + data.Count + " with time: " + Math.floor(Date.now() / 1000));
            console.log("DEBUG DB QUERY ALL: lastEvaluatedKey--- " + JSON.stringify(lastEvaluatedKey));
            console.log("DEBUG DB QUERY ALL: itemsAll length--- " + itemsAll.Items.length);
            if (lastEvaluatedKey) {
                params.ExclusiveStartKey = lastEvaluatedKey;
            }
        }

        itemsAll.LastEvaluatedKey = lastEvaluatedKey
        return itemsAll;
    } catch (error) {
        console.log("DEBUG DB QUERY ALL ERROR: " + error);
        return ""
    }
};

module.exports.queryALL = async (params) => {
    let lastEvaluatedKey = 'dummy'; // string must not be empty
    const itemsAll = {
        Items: [],
        Count: 0
    };
    let queryCount = 1;
    try {
        while (lastEvaluatedKey) {
            console.log("DEBUG DB QUERY ALL ---  " + queryCount + "th WITH PARAMS: " + JSON.stringify(params));
            queryCount += 1;
            const data = await client.query(params).promise();
            itemsAll.Items.push(...data.Items);
            itemsAll.Count += data.Count;
            lastEvaluatedKey = data.LastEvaluatedKey;
            console.log(`DEBUG DB QUERY ALL: data.Count---: ${data.Count} ----with time: ${Math.floor(Date.now() / 1000)} ----lastEvaluatedKey: ${JSON.stringify(lastEvaluatedKey)} ----itemsAll length: ${itemsAll.Items.length}`);
            if (lastEvaluatedKey) {
                params.ExclusiveStartKey = lastEvaluatedKey;
            }
        }
        return itemsAll;
    } catch (error) {
        console.log("DEBUG DB QUERY ALL ERROR: " + error);
        return ""
    }
};
module.exports.scanALL = async (params) => {
    let lastEvaluatedKey = 'dummy'; // string must not be empty
    const itemsAll = {
        Items: [],
        Count: 0
    };
    let scanCount = 1;
    try {
        while (lastEvaluatedKey) {
            console.log("DEBUG DB SCAN ALL ---  " + scanCount + "th WITH PARAMS: " + JSON.stringify(params));
            scanCount += 1;
            const data = await client.scan(params).promise();
            itemsAll.Items.push(...data.Items);
            itemsAll.Count += data.Count;
            lastEvaluatedKey = data.LastEvaluatedKey;
            console.log("DEBUG DB SCAN ALL: data.Count--- " + data.Count + " with time: " + Math.floor(Date.now() / 1000));
            console.log("DEBUG DB SCAN ALL: lastEvaluatedKey--- " + JSON.stringify(lastEvaluatedKey));
            console.log("DEBUG DB SCAN ALL: itemsAll length--- " + itemsAll.Items.length);
            if (lastEvaluatedKey) {
                params.ExclusiveStartKey = lastEvaluatedKey;
            }
        }
        return itemsAll;
    } catch (error) {
        console.log("DEBUG DB SCAN ALL ERROR: " + error);
        return ""
    }
};
module.exports.describeTable = (params) => {
    return new Promise((resolve) => {
        dynamo.describeTable(params, (err, data) => {
            if (err) resolve();
            else {
                console.debug("DEBUG DB DESCRIBE SUCCESSFUL: " + params.TableName);
                resolve(data);
            }
        });
    });
};

module.exports.describeTTL = (params) => {
    return new Promise((resolve) => {
        dynamo.describeTimeToLive(params, (err, data) => {
            if (err) resolve();
            else {
                console.debug("DEBUG DB DESCRIBE TIME SUCCESSFUL: " + params.TableName);
                resolve(data);
            }
        });
    });
};
