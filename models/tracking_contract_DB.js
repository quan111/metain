const DbHelper = require('./db');

const tableName = `tracking_contract`
const primaryKey = 'contract_id';
const rangeKey = 'wallet_id_time';
const att1 = 'wallet_id';
const att2 = 'time';
const att3 = 'eth_count';

const getParam = (primaryKeyalue, rangeKeyValue) => {
    var key = {};
    key[primaryKey] = primaryKeyalue;
    key[rangeKey] = rangeKeyValue;
    return {
        TableName: tableName,
        Key: key
    };
};

const get = (primaryKeyalue, rangeKeyValue) => {
    return DbHelper.get(getParam(primaryKeyalue, rangeKeyValue));
};

const deleteItem = (primaryKeyalue, rangeKeyValue) => {
    return DbHelper.delete(getParam(primaryKeyalue, rangeKeyValue));
};

const putParam = (primaryKeyalue, rangeKeyValue, att1Val, att2Val, att3Val) => {
    var items = {};
    items[primaryKey] = primaryKeyalue;
    items[rangeKey] = rangeKeyValue;
    if (att1Val)
        items[att1] = att1Val;
    if (att2Val)
        items[att2] = att2Val;
    if (att3Val)
        items[att3] = att3Val;
    return {
        TableName: tableName,
        Item: items
    };
};

const put = (primaryKeyalue, rangeKeyValue, att1Val, att2Val, att3Val) => {
    return DbHelper.put(putParam(primaryKeyalue, rangeKeyValue, att1Val, att2Val, att3Val));
};

const scanAllParam = () => {
    return {
        TableName: tableName
    };
};

const scanAll = () => {
    return DbHelper.scanALL(scanAllParam());
};

const queryByWalletParam = (primaryKeyValue, wallet_id) => {
    const queryExpress = primaryKey + " = :primaryKeyValue";
    const queryFilter = "#wallet_id = :wallet_id_value";
    const queryValue = {
        ":primaryKeyValue": primaryKeyValue,
        ":wallet_id_value": wallet_id
    }
    const queryName = {
        "#wallet_id": att1
    }

    return {
        TableName: tableName,
        KeyConditionExpression: queryExpress,
        FilterExpression: queryFilter,
        ExpressionAttributeValues: queryValue,
        ExpressionAttributeNames: queryName,
    };
};

const queryByWallet = (project_id, privilege) => {
    return DbHelper.queryALL(queryByWalletParam(project_id, privilege));
};

module.exports = {
    put: put,
    get: get,
    delete: deleteItem,
    scanAll: scanAll,
    queryByWallet: queryByWallet,
    primaryKey: primaryKey,
    rangeKey: rangeKey,
    wallet_id: att1,
    time: att2,
    eth_count: att3
}