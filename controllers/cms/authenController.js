
const console           = require('../../helpers/console_log');

const login = async (req, res) => {
    try {
        const username = req.body.username;

        res.json({
            response: 'Login successfully'
        });
        
    } catch (err) {
        console.log('Debug ============= login: exception: ' + err);
        return res.status(500).json({error: 'Server error'})
    }
}

const register = async (req, res) => {
    try {
        const username = req.body.username;

        res.json({
            response: 'register successfully'
        });
        
    } catch (err) {
        console.log('Debug ============= register: exception: ' + err);
        return res.status(500).json({error: 'Server error'})
    }
}

module.exports = {
    login,
    register
}  