const console = require('../../helpers/console_log');
const authen = require('../../helpers/authen/api-auth');
const security = require('../../helpers/security');

const getContractInfo = async(req, res, next) => {
    try {
        const version = req.body.version;
        if (!security.checkDataInput(version, security.DATA_TYPE_STRING, security.DATA_LENGTH_GE, 1))
            return goNext(400, {
                error: 'BODY_WRONG_FORMAT'
            }, res, next);

        const auth = await authen.apiValidateAPI(req.get('x-access-token'));

    } catch (err) {
        console.log('Debug ------------------- getContractInfo: exception: ' + err);
        return goNext(500, {
            error: 'Server error'
        }, res, next)
    }
}

const setContractInfo = async(req, res, next) => {
    try {
        const version = req.body.version;
        if (!security.checkDataInput(version, security.DATA_TYPE_STRING, security.DATA_LENGTH_GE, 1))
            return goNext(400, {
                error: 'BODY_WRONG_FORMAT'
            }, res, next);

        const auth = await authen.apiValidateAPI(req.get('x-access-token'));


    } catch (err) {
        console.log('Debug ------------------- setContractInfo: exception: ' + err);
        return goNext(500, {
            error: 'Server error'
        }, res, next);
    }
}

const goNext = (status, msg, res, next) => {
    res.statusCode = status
    res.msg = msg
    next()
}

module.exports = {
    getContractInfo,
    setContractInfo
}