// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

contract Test{

    event update_data(address wallet, uint ethCount, uint sum, uint count);

    event update_test(address wallet, uint sum, uint count);

    uint public sum;
    uint public count;

    function send_ETH() public payable{
        sum = sum + msg.value;
        count = count + 1;
        emit update_data(msg.sender, msg.value, sum, count);
    }

    function test_update() public{
        sum = sum + 10;
        count = count + 1;
        emit update_test(msg.sender, sum, count);
    }


}